# UniverSIS
[UniverSIS](https://universis.gr) is a coordinated effort by Greek academic institutions to build a Student Information System as an open source platform. The target is to serve our common needs to support academic and administrative processes.

## UniverSIS-students
The UniverSIS platform consists of a number of modules. The Students front-end is an interface for all student interaction.

* Student application features
* Student dashboard for current status and courses/exams
* Student personal profile info
* Semester registration
* Semester courses enrollment
* Recent grades
* Full grades transcript
* Thesis info
* Applications submission to the Registrar

## Demo
UniverSIS student demo application is available here: https://students.universis.io

You can use the following student credentials to test various cases:

* **6th semester active student**
student3@example.com           
7B18D8EA-DF81-4ADB-8D90-BAF5C3DD3614

* **4th semester active student**
student5@example.com              
F1FDFCCF-9E1A-4F68-950D-F010FB28C940

* **10th semester active student**
student7@example.com              
BBC67EA1-81A4-4464-9602-74E72DCF16BE

* **graduated student**
student6@example.com              
35270474-35B3-47F7-B284-DF4A4D327C8B

## How to install
  Installation instructions can be found in the [installation file](INSTALL.md)

## How to contribute

Developers can check out our [contributing guide](CONTRIBUTING.md).

We use Gitlab to track bugs and feature requests for the code. When submitting a bug please make sure you submit a comprehensive description and all relevant information.

## Documentation

The documentation is under constant development.
Check the documentation section at https://gitlab.com/universis/universis-api

## Contact
If you are interested in participating in the development efforts, please contact us at **info@universis.gr**

## Questions?

Check our wiki [FAQ](https://gitlab.com/universis/universis.io/wikis/FAQ) page, and feel free to [contact us](https://www.universis.gr/#contact).
